/*
    BNF

    regex -> concatenacion R1
    R1 -> | concatenacion R1
    R1 -> e
    concatenacion -> grupo R2
    R2 -> concatenacion R2
    R2 -> e
    grupo -> elemento operador
    operador -> *
    operador -> +
    operador -> e
    elemento -> ∑
    elemento -> ( regex )
*/

function RegexParser(regex, alfabeto) {
  this.expresion = regex;
  this.alfabeto = alfabeto;
  this.error = false;
  this.posicion = 0;
  this.estados = 0;

  this.match = function(token) {
    if (token == this.expresion[0]) {
      this.expresion = this.expresion.slice(1);
      this.posicion++;
    } else if (this.expresion == "") {
      this.error = true;
      console.log("Final de cadena inesperado en la posicion " + this.posicion);
    } else {
      this.error = true;
      console.log("Token inesperado en la posicion " + this.posicion);
    }
  };

  this.analizar = function() {
    var nfa = this.regex();
    nfa.alfabeto = this.alfabeto;
    return nfa;
  };

  /*
   *
   * Funciones de anlisis sintáctico
   *
   */

  this.regex = function() {
    if (this.error) return;

    console.log("regex: " + this.expresion);

    token = this.expresion[0];
    if (this.alfabeto.indexOf(token) > -1 || token == "(") {
      var nfa = this.concatenacion();
      nfa = this.R1(nfa);
    }
    return nfa;
  };

  this.R1 = function(nfa) {
    if (this.error) return;

    console.log("R1: " + this.expresion);

    token = this.expresion[0];
    if (token == "|") {
      this.match("|");
      var nfa2 = this.concatenacion();
      nfa2 = this.R1(nfa2);
      // Se aplica Thompson para union
      nfa.unir(nfa2);
    }
    return nfa;
  };

  this.concatenacion = function() {
    if (this.error) return;

    console.log("concatenacion: " + this.expresion);

    token = this.expresion[0];
    var nfa = this.grupo();
    nfa = this.R2(nfa);

    return nfa;
  };

  this.R2 = function(nfa) {
    if (this.error) return;

    console.log("R2: " + this.expresion);

    token = this.expresion[0];
    if (this.alfabeto.indexOf(token) > -1 || token == "(") {
      var nfa2 = this.concatenacion();
      nfa2 = this.R2(nfa2);
      // Se aplica Thompson para concatenación
      nfa.concatenar(nfa2);
    }
    return nfa;
  };
  this.grupo = function() {
    if (this.error) return;

    console.log("grupo: " + this.expresion);

    token = this.expresion[0];
    var nfa = this.elemento();
    nfa = this.operador(nfa);

    return nfa;
  };
  this.operador = function(nfa) {
    if (this.error) return;

    console.log("operador: " + this.expresion);

    token = this.expresion[0];
    if (token == "*") {
      this.match("*");
      // Se aplica Thompson para Kleene-Star
      nfa.kleene();
    } else if (token == "+") {
      this.match("+");
      // Se aplica Thompson para operador positivo
      nfa.positivo();
    }
    return nfa;
  };
  this.elemento = function() {
    if (this.error) return;

    console.log("elemento: " + this.expresion);

    token = this.expresion[0];
    if (this.alfabeto.indexOf(token) > -1) {
      this.match(token);
      //Se crea el NFA básico
      var nfa = new NFA();
      nfa.simpleNFA(token);
    } else if (token == "(") {
      this.match("(");
      var nfa = this.regex();
      this.match(")");
    }

    return nfa;
  };
}
