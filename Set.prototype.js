Set.prototype.equals = function(set) {
  if (this.size !== set.size) return false;
  for (var a of this) if (!set.has(a)) return false;
  return true;
};
Set.prototype.cerraduraE = function() {
  cerradura = new Set();
  this.forEach(e => {
    cerradura = new Set([...cerradura, e.cerraduraE()]);
  });
  return cerradura;
};
