function Estado() {
  this.label;
  this.automata;
  this.transiciones = [];
  this.esFinal = false;
  this.visitado = false;

  this.mover = function(simbolo) {
    for (let i = 0; i < this.transiciones.length; i++) {
      if (this.transiciones[i].simbolo == simbolo)
        return this.transiciones[i].hasta;
    }
    return new Set();
  };
  this.cerraduraE = function() {
    var cerradura = new Set([this]);
    for (let i = 0; i < this.transiciones.length; i++) {
      if (this.transiciones[i].simbolo == "epsilon") {
        aux = this.transiciones[i].hasta.cerraduraE();
        cerradura = new Set([...cerradura, ...aux]);
      }
    }
    return cerradura;
  };
}
