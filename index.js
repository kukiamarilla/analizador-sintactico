// create an array with nodes
var nodes = new vis.DataSet([]);

// create an array with edges
var edges = new vis.DataSet([]);

// create a network
var containerNFA = document.getElementById("nfa");
var containerDFA = document.getElementById("nfa");

// provide the data in the vis format
var data = {
  nodes: nodes,
  edges: edges
};
var options = {};

// initialize your network!
var network = new vis.Network(containerNFA, data, options);
var network = new vis.Network(containerDFA, data, options);

input = document.querySelector("[name=regex]");
input.onkeyup = function() {
  var regex = this.value;
  var alfabeto = document.querySelector("[name=alfabeto]").value;
  nfa = new RegexParser(regex, alfabeto).analizar();
  nfa.draw();
  dfa = new DFA();
  dfa.parse(nfa);
  dfa.draw();
};

button = document.querySelector("#noThompson");
button.onclick = function() {
  nfa = new NFA();
  nfa.noThompson(5);
  nfa.draw();
  dfa = new DFA();
  dfa.parse(nfa);
  dfa.draw();
};
