function DFA() {
  this.estados = [];
  this.inicio;
  this.finales = [];
  this.alfabeto = "abc";

  this.draw = function() {
    this.contador = 0;
    var estados = [];
    var transiciones = [];
    for (let i = 0; i < this.estados.length; i++) {
      this.estados[i].visitado = false;
    }
    this.recorrer(this.inicio, estados, transiciones);
    estados.push({ id: 1000, color: "#fff" });
    transiciones.push({
      from: 1000,
      to: this.inicio.label,
      label: "inicio",
      arrows: "to",
      color: "rgb(43, 124, 233)"
    });

    var nodes = new vis.DataSet(estados);
    var edges = new vis.DataSet(transiciones);
    var container = document.getElementById("dfa");
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {};
    var network = new vis.Network(container, data, options);
  };

  this.recorrer = function(estado, estados, transiciones) {
    if (estado.visitado) return estado.label;
    estado.visitado = true;
    this.contador++;
    estado.label = this.contador;
    if (estado.esFinal) {
      estados.push({
        id: this.contador,
        label: this.contador.toString(),
        color: "#5da76a",
        shapeProperties: { borderDashes: [5, 5] }
      });
    } else {
      estados.push({ id: this.contador, label: this.contador.toString() });
    }

    for (let i = 0; i < estado.transiciones.length; i++) {
      var to = this.recorrer(
        estado.transiciones[i].hasta,
        estados,
        transiciones
      );
      transiciones.push({
        from: estado.label,
        to: to,
        label: estado.transiciones[i].simbolo,
        arrows: "to"
      });
    }
    return estado.label;
  };
  this.parse = function(nfa) {
    let estado = new Estado();
    estado.label = 0;
    transicion = new Transicion();
    transicion.simbolo = "epsilon";
    transicion.desde = estado;
    transicion.hasta = nfa.inicio;
    estado.transiciones.push(transicion);
    nfa.estados.push(estado);
    nfa.inicio = estado;

    this.alfabeto = nfa.alfabeto;
    map = [];

    conj = nfa.inicio.cerraduraE();
    contador = 1;
    nuevoEstado = new Estado();
    nuevoEstado.automata = this;
    nuevoEstado.label = contador.toString();
    this.estados.push(nuevoEstado);
    this.inicio = nuevoEstado;
    nuevo = { conjunto: conj, estado: nuevoEstado };
    map.push(nuevo);
    i = 0;
    while (i < map.length) {
      for (let k = 0; k < this.alfabeto.length; k++) {
        conj = new Set();
        map[i].conjunto.forEach(e => {
          conj = new Set([...conj, ...e.mover(this.alfabeto[k]).cerraduraE()]);
        });
        existe = false;
        for (let j = 0; j < contador; j++) {
          if (conj.equals(map[j].conjunto)) {
            transicion = new Transicion();
            transicion.desde = map[i].estado;
            transicion.hasta = map[j].estado;
            transicion.simbolo = this.alfabeto[k];
            map[i].estado.transiciones.push(transicion);
            existe = true;
          }
        }
        if (!existe) {
          contador++;
          nuevoEstado = new Estado();
          nuevoEstado.automata = this;
          nuevoEstado.label = contador.toString();
          nuevo = { conjunto: conj, estado: nuevoEstado };
          map.push(nuevo);
          this.estados.push(nuevoEstado);
          esFinal = false;
          conj.forEach(el => {
            if (el.esFinal) esFinal = true;
          });
          nuevoEstado.esFinal = esFinal;

          transicion = new Transicion();
          transicion.desde = map[i].estado;
          transicion.hasta = nuevoEstado;
          transicion.simbolo = this.alfabeto[k];
          map[i].estado.transiciones.push(transicion);
        }
      }
      i++;
    }
  };
}
