function NFA() {
  this.estados = [];
  this.inicio;
  this.finales = [];
  this.alfabeto = "abc";
  this.actuales;
  this.contador;

  this.draw = function() {
    this.contador = 0;
    var estados = [];
    var transiciones = [];
    for (let i = 0; i < this.estados.length; i++) {
      this.estados[i].visitado = false;
    }
    this.recorrer(this.inicio, estados, transiciones);
    estados.push({ id: 1000, color: "#fff" });
    transiciones.push({
      from: 1000,
      to: this.inicio.label,
      label: "inicio",
      arrows: "to",
      color: "rgb(43, 124, 233)"
    });

    var nodes = new vis.DataSet(estados);
    var edges = new vis.DataSet(transiciones);
    var container = document.getElementById("nfa");
    var data = {
      nodes: nodes,
      edges: edges
    };
    var options = {};
    var network = new vis.Network(container, data, options);
  };

  this.recorrer = function(estado, estados, transiciones) {
    if (estado.visitado) return estado.label;
    estado.visitado = true;
    this.contador++;
    estado.label = this.contador;
    if (estado.esFinal) {
      estados.push({
        id: this.contador,
        label: this.contador.toString(),
        color: "#5da76a",
        shapeProperties: { borderDashes: [5, 5] }
      });
    } else {
      estados.push({ id: this.contador, label: this.contador.toString() });
    }

    for (let i = 0; i < estado.transiciones.length; i++) {
      var to = this.recorrer(
        estado.transiciones[i].hasta,
        estados,
        transiciones
      );
      transiciones.push({
        from: estado.label,
        to: to,
        label: estado.transiciones[i].simbolo,
        arrows: "to"
      });
    }
    return estado.label;
  };
  this.mover = function(simbolo) {
    estados = this.actuales;
    nuevos = [];
    for (let i = 0; i < estados.length; i++) {
      for (let j = 0; j < estados[i].transiciones.length; j++) {
        if (
          estados[i].transiciones[j].simbolo == simbolo ||
          estados[i].transiciones[j].simbolo == "epsilon"
        ) {
          nuevos.add(estados[i].transiciones[j].hasta);
        }
      }
    }
    return nuevos;
  };

  this.simpleNFA = function(token) {
    this.estados = [];
    this.finales = [];

    var inicial = new Estado();
    var final = new Estado();

    this.estados.push(inicial);
    this.estados.push(final);

    var transicion = new Transicion();
    transicion.desde = inicial;
    transicion.hasta = final;
    transicion.simbolo = token;
    inicial.transiciones.push(transicion);

    this.inicio = inicial;
    this.finales.push(final);
    final.esFinal = true;

    inicial.automata = this;
    final.automata = this;
  };

  this.concatenar = function(nfa) {
    for (let i = 0; i < nfa.estados.length; i++) {
      this.estados.push(nfa.estados[i]);
      nfa.estados[i].automata = this;
    }
    for (let i = 0; i < this.finales.length; i++) {
      var transicion = new Transicion();
      transicion.desde = this.finales[i];
      transicion.hasta = nfa.inicio;
      transicion.simbolo = "epsilon";
      this.finales[i].transiciones.push(transicion);
      this.finales[i].esFinal = false;
    }
    this.finales = nfa.finales;
  };

  this.unir = function(nfa) {
    for (let i = 0; i < nfa.estados.length; i++) {
      this.estados.push(nfa.estados[i]);
      nfa.estados[i].automata = this;
    }
    var inicial = new Estado();
    var final = new Estado();

    inicial.automata = this;
    final.automata = this;
    this.estados.push(inicial);
    this.estados.push(final);

    var transicionI1 = new Transicion();
    transicionI1.desde = inicial;
    transicionI1.hasta = this.inicio;
    transicionI1.simbolo = "epsilon";

    var transicionI2 = new Transicion();
    transicionI2.desde = inicial;
    transicionI2.hasta = nfa.inicio;
    transicionI2.simbolo = "epsilon";

    inicial.transiciones.push(transicionI1);
    inicial.transiciones.push(transicionI2);

    for (let i = 0; i < this.finales.length; i++) {
      var transicionF1 = new Transicion();
      transicionF1.desde = this.finales[i];
      transicionF1.hasta = final;
      transicionF1.simbolo = "epsilon";
      this.finales[i].esFinal = false;
      this.finales[i].transiciones.push(transicionF1);
    }

    for (let i = 0; i < nfa.finales.length; i++) {
      var transicionF2 = new Transicion();
      transicionF2.desde = nfa.finales[i];
      transicionF2.hasta = final;
      transicionF2.simbolo = "epsilon";
      nfa.finales[i].esFinal = false;
      nfa.finales[i].transiciones.push(transicionF2);
    }

    this.inicio = inicial;
    this.finales = [];
    this.finales.push(final);
    final.esFinal = true;
  };

  this.kleene = function() {
    var inicial = new Estado();
    var final = new Estado();

    this.estados.push(inicial);
    this.estados.push(final);
    inicial.automata = this;
    final.automata = this;
    for (let i = 0; i < this.finales.length; i++) {
      var transicion = new Transicion();
      transicion.desde = this.finales[i];
      transicion.hasta = this.inicio;
      transicion.simbolo = "epsilon";
      this.finales[i].transiciones.push(transicion);

      transicion = new Transicion();
      transicion.desde = this.finales[i];
      transicion.hasta = final;
      transicion.simbolo = "epsilon";
      this.finales[i].transiciones.push(transicion);

      this.finales[i].esFinal = false;
    }

    transicion = new Transicion();
    transicion.desde = inicial;
    transicion.hasta = final;
    transicion.simbolo = "epsilon";
    inicial.transiciones.push(transicion);

    transicion = new Transicion();
    transicion.desde = inicial;
    transicion.hasta = this.inicio;
    transicion.simbolo = "epsilon";
    inicial.transiciones.push(transicion);

    this.inicio = inicial;
    this.finales = [];
    this.finales.push(final);

    final.esFinal = true;
  };

  this.positivo = function() {
    var inicial = new Estado();
    var final = new Estado();

    this.estados.push(inicial);
    this.estados.push(final);

    for (let i = 0; i < this.finales.length; i++) {
      var transicion = new Transicion();
      transicion.desde = this.finales[i];
      transicion.hasta = this.inicio;
      transicion.simbolo = "epsilon";
      this.finales[i].transiciones.push(transicion);

      transicion = new Transicion();
      transicion.desde = this.finales[i];
      transicion.hasta = final;
      transicion.simbolo = "epsilon";
      this.finales[i].transiciones.push(transicion);

      this.finales[i].esFinal = false;
    }

    transicion = new Transicion();
    transicion.desde = inicial;
    transicion.hasta = this.inicio;
    transicion.simbolo = "epsilon";
    inicial.transiciones.push(transicion);

    this.inicio = inicial;
    this.finales = [];
    this.finales.push(final);
    final.esFinal = true;
  };

  this.noThompson = function(estados) {
    this.estados = [];
    let incialesPosibles = [];
    for (let i = 0; i < estados; i++) {
      estado = new Estado();
      estado.label = i + 1;
      estado.automata = this;
      estado.esFinal = Math.round(Math.random()) == 1;
      if (estado.esFinal) {
        this.finales.push(estado);
      }
      this.estados.push(estado);
    }
    for (let i = 0; i < estados; i++) {
      transiciones = Math.floor(Math.random() * 5);
      for (let j = 0; j < transiciones; j++) {
        let transicion = new Transicion();
        transicion.desde = estado;
        transicion.hasta = this.estados[Math.floor(Math.random() * estados)];
        incialesPosibles.push(transicion.hasta);
        simbolo = Math.floor(Math.random() * (this.alfabeto.length + 1));
        transicion.simbolo =
          simbolo == this.alfabeto.length ? "epsilon" : this.alfabeto[simbolo];

        this.estados[i].transiciones.push(transicion);
      }
    }
    this.inicio =
      incialesPosibles[Math.floor(Math.random() * incialesPosibles.length)];
  };
}
